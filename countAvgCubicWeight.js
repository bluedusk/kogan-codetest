const axios = require('axios');


/**
 * key: url postfix
 * value: cubic weights for certain category [weight1, weight2,....]
 */
const url_weights_map = new Map();

/**
 * Count average weight of fetched data
 * @param {Map} map result of fetchdata
 */
const processData = (map) => {
    if(!map){
        return;
    }
    const it = map.values();
    let result = [0, 0];

    for (let cubicWeights of map.values()) {
        for (let ele of cubicWeights) {
            result[0] += ele;
            result[1]++;
        }
    }

    return result[1] === 0 ? 0 : (result[0] / result[1]).toFixed(2);
}

/**
 * 
 * Fetch Url data recursively
 * 
 * - fetchConfig: config about url/category/etc
 * 
 * - fetchAll: 
 *  - true: fetch all pages
 *  - false: fetch current page
 * 
 */
const countAvg = async (fetchConfig, fetchAll = true) => {

    fetchData = async (url) => {

        try {
            const response = await axios.get(`${fetchConfig.apiPrefix}${url}`);
            if (!response.data) {
                return url_weights_map;
            }
            const nextUrl = response.data.next || null;
            const objects = response.data.objects || [];
            // cubicWeights = [weight1, weight2, ...]
            const cubicWeights = objects.filter(item => item.category === fetchConfig.category).map(item => {
                const { width, length, height } = item.size;
                item = width * length * height / 1000000 * 250;
                return item;
            })
            console.log(url, cubicWeights);
            url_weights_map.set(url, cubicWeights);

            // if(last page || infinite loop) exit function 
            if (!fetchAll || nextUrl === null || url_weights_map.has(nextUrl)) {
                return url_weights_map;
            }
            return await fetchData(nextUrl);

        } catch (e) {
            // console.error(`[Error in fetchData] -> ${e.message}`);
            return Promise.reject({message: `[Error in fetchData] -> ${e.message}`});
        }   
    }
    let result = 0;

    try {
        let map = await fetchData(fetchConfig.entryPiont);
        result = processData(map);
    } catch (e) {
        // console.error(`[Error in countAvg] -> ${e.message}`);
        return Promise.reject({message: `[Error in countAvg] -> ${e.message}`});

    }
    return result;
}

module.exports.countAvg = countAvg;