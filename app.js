const { countAvg } = require("./countAvgCubicWeight");
const config = require('./config.js');

countAvg(config, true).then((result) => {
    console.log('\x1b[33m', `THE AVERAGE CUBIC WEIGHT IS : ${result} KG`)
}).catch(e => {
    console.error(`[Error in App] -> ${e.message}`);
})