const expect = require('chai').expect;
const { countAvg } = require("./countAvgCubicWeight");
const config = require('./config');


/**
 * Count Average Cubic TEST CASES
 */
describe("TEST CASE : Count Average Cubic", function () {

    it("return avage cubic weight", function (done) {
       
        countAvg(config, true).then(
           
            result => {
                expect(result).to.equal('41.61');
                done();
            }
        );
    }).timeout(10000);
});
