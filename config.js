let env = process.env.NODE_ENV || 'test';
let config = require('./config.json');


let envConfig = config.test; 

if(env === "dev"){
    envConfig = config.dev;
}

module.exports = envConfig;