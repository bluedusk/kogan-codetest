# installation

- nodejs 
recommended version: v8.11.2   
https://nodejs.org/en/   

- enter folder Kogan
- run command: npm i

# to run 

- npm start

# to test

- npm test

# thanks you

Dan Zhu   
danielhymn@gmail.com